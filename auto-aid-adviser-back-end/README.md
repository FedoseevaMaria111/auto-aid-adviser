#Auto Aid Adviser
Backend for Auto Aid Adviser project

##Building project
JDK 11 need to be installed

From the root dir of a project run

- Windows: 
```gradlew.bat bootJar```
- Mac, Linux: 
```./gradlew bootJar```

to skip test phase add `-x test` to the previous command.

##Run project

```java -jar auto-aid-adviser-main/build/libs/auto-aid-adviser-main.jar```
